<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();
header('Content-type: text/html; charset=utf-8');

?>


<html>
    <head>
        <title> Kiva Peli </title>
        <link type="text/css" rel="stylesheet" href="GameFormatting.css"/>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    </head>
    <body>
        
        <div id="header">
            <h1>Kirjautuminen</h1>

            <br><br>
            
            <form action="https://www-ht-pasilehtinen.c9users.io/HT/newuser.php" method="post">
                    <input type="submit" value ="Rekisteröidy"/>
            </form>

            <a><?php echo  $_SESSION['loginmessage']?></a>
            <br><br>
            
            <div>
                <form name = "login" action="https://www-ht-pasilehtinen.c9users.io/HT/LoginSubscribeHandler.php" method="post">
                    Käyttäjätunnus: <input type="text" name="username"></input>
                    Salasana: <input type="password" name="password"></input>
                    <input type="submit"/>
                </form>
            </div>
            <div>
                <a>
                Kiva astronauttipeli! Sinun pitää kerätä tähtipölyä universumin pelastamiseksi. 
                Varo lähestyviä meterotiitteja tai aluksesi tuhoutuu. Pääsetkö maaliin asti? Muista, että avaruus voi tuoda 
                eteesi myös vihamielisiä avaruusolioita! Kokeile kuinka hyvä astronautti sinä olisit!
                </a>
                <br><br>
                <a>
                Ota selvää kuka oli maailman ensimmäinen astronautti, joka päihitti pahan avaruusolion! 
                Voit myös otella kaveriasi vastaan kaksintaistelussa ja todistaa omat kykysi muille astronauteille. 
                Parhaiden pelaajien nimet julkistetaan astronauttien Wall of Famessa!
                </a>
            </div>
        </div>
        
    </body>
</html>
