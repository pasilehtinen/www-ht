<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();


if(!isset($_SESSION['uid']))
{
    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/login.php");
    exit();
    /*If user has not logged in they can't see this site --> Automatically redirect to login site*/

}

header('Content-type: text/html; charset=utf-8');
    

        $db = new PDO('mysql:host=localhost; dbname=www; charset=utf8', 'pasilehtinen');
        $stmt1 = $db->prepare("
        Select max(w) as w, max(l) as l, level from
        (
        (Select ifnull(w,0) as w, ifnull(l, 0) as l, (case when (case when W.level is not null then W.level else L.level end) = 0 then 10 else
        (case when W.level is not null then W.level else L.level end) end) as level from 
            (select count(*) as w, level from GameStatistics where result = 1 and uid = (:value1) group by level) W
            left join (select count(*) as l, level from GameStatistics where result = 0 and uid = (:value1) group by level) L
            on W.level = L.level)
        union all
        (Select ifnull(w, 0) as w, ifnull(l, 0) as l, (case when (case when W.level is not null then W.level else L.level end) = 0 then 10 else
        (case when W.level is not null then W.level else L.level end) end) as level from 
            (select count(*) as w, level from GameStatistics where result = 1 and uid = (:value1) group by level) W
            right join (select count(*) as l, level from GameStatistics where result = 0 and uid = (:value1) group by level) L
            on W.level = L.level where W.level is null)
        union all
        (Select 0 as w, 0 as l, (case when level = 0 then 10 else level end) as level 
        from GameStatistics where uid = (:value1) and result is null)
        ) t
        group by level
        order by level asc");
        /*Since both losts and wons are recorded into same column a self join is required 
        in order to aggregate rows separately based on the result of the game (win / loose);
        A problem might occur when either win or loose result has not yet occurred and result set is empty
        --> This could be fixed by using outer join.
        MySQL syntax doesn't have full outer join 
        --> in order to select all records there will be used 2 record sets and combine these with union*/
        /*also only retrive results from levels 1-10 but taking into account that level 10 results are stored with value 0*/
        /*The second union combines real results with dummy rows 
        (dummy rows created when new user subscribes and the result is null for each level)*/
        /*The last part is to eliminate duplicate rows which are created because of the dummy union
            - This requires "dummy" aggregation with max() function and group by clause*/
        $stmt1->execute(array(":value1" => $_SESSION['uid']));
        $rows1 = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    
        print(json_encode($rows1));

?>