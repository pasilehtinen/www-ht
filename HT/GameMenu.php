<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();
if(!isset($_SESSION['uid']))
{
    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/login.php");
    exit();
    
    /*If user has not logged in they can't see this site --> Automatically redirect to login site*/
}
header('Content-type: text/html; charset=utf-8');
?>


<html>
    <head>
        <title> Kiva Peli </title>
        <meta name="description" content ="Kiva astronauttipeli, missä sinun tulee väistellä meteroriitteja, 
        kerätä tähtipölyä ja tuhota vihollisia! Kerää pisteitä! Tuhoa vihollisia! Meteoriitti lähestyy - Ehditkö väistämään! 
        Parhaiden pelaajien tulokset tallentuvat Wall of Famelle! 
        Kokeile kuinka hyvä astronautti sinä olisit!"/>
        <link type="text/css" rel="stylesheet" href="GameFormatting.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="scripts/jquery.js"></script>
        
    <script type="text/javascript">
        function loadStatisticsjQuery() {
            /*Retrieve game statistics - function has 3 parts:
                1) First player who has defeated the last boss
                2) Top10 players based on the survival game (most star dusts collected)
                3) Player's own statistics
            */
            
            $.getJSON("FirstHero.php", function(hero) {
                
                var text = "<ul>";
                    text += "<li>"+hero+"</li> ";
                text += "</ul>";
                
                document.getElementById("firstPlayerDefeatedBoss").innerHTML = "<a>"+text+"</a>";
            });
            
            $.getJSON("GameResults.php", function(data) {
                var text = "<ol>";
                data.forEach(function(entry) {
                    text += "<li ID="+entry.ID+">" + entry.username +
                            " on kerännyt " + entry.level + " tähtipölyhiukkasta yhdellä avaruuslennolla!"+
                    "</li> ";
                });
                text += "</ol>";
                document.getElementById("gamestatisticsAJAX").innerHTML = text;
            });
            $.getJSON("OwnStatistics.php", function(stats) {
                var text = "<ul>";
                stats.forEach(function(result) {
                    text += "<li> Level " + result.level + ": Voitot ("+result.w+") - Häviöt ("+result.l+")</li> ";
                });
                text += "</ul>";
                document.getElementById("ownStats").innerHTML = text;
            });
        }
   

        

            $(document).ready(function() {
                loadStatisticsjQuery();
            });
            function checkInput() {
                /*Only numerical values larger than 0 are allowed*/
                var x=document.forms["gameLevel"]["level"].value;
                if (isNaN(x) || x == '' ) {
                    document.getElementById("alert").innerHTML = "Vaikeustason tulee olla numero!";
                    return false;
                }
                if (x<=0) {
                    document.getElementById("alert").innerHTML = "Vaikeustason tulee olla suurempi kuin nolla!";
                    return false;
                }
                if (x > 11) {
                    document.getElementById("alert").innerHTML = "Viimeinen taso on level 11!";
                    return false;
                }
                
            }
        </script>
        
    </head>
    <body>
        <div id="header">
            <h1>Kiva Peli</h1>
            <br>
            <a><?php if(isset($_SESSION['uname'])) {echo $_SESSION['uname'];} ?></a>
            
            <form action="https://www-ht-pasilehtinen.c9users.io/HT/logout.php"} method="post">
                    <input type="submit" value ="Kirjaudu ulos"/>
            </form>
        
            <div id="alert"><br></div>
            <form name="gameLevel" action="https://www-ht-pasilehtinen.c9users.io/HT/GamePVE2.php" onsubmit="return checkInput()" method="post">
                Valitse vaikeusaste: <input type="text" name="level"  placeholder="level"></input>
                <input type="submit" value="Pelaa (PVE)"/>
            </form>
            
            <form name="pvp_offline" action="https://www-ht-pasilehtinen.c9users.io/HT/GamePVP2.php" method="post">
                <input type="submit" value="Pelaa (PVP)"/>
            </form>
            
        </div>    
        
        <br><br><br><br><br><br><br><br>
        <a><H3>Maailman ensimmäinen astronautti, joka tuhosi vikan bossin:</H3></a>
        <div id="firstPlayerDefeatedBoss"></div>
        <a><H3>Top 10 astronautit:</H3></a>
        <div id="gamestatisticsAJAX"></div>
        <a><H3>Omat tilastot:</H3></a>
        <div id="ownStats"></div>
        
    </body>
</html>