<!--Created by Pasi Lehtinen 05/12/15-->
<?php
session_start();

header('Content-type: text/html; charset=utf-8');

    if(isset($_POST['newGameResult']) && isset($_POST['level'])) {
        /*This will be called when game ends (win/loose) and the result will be stored into the database*/
        $db = new PDO('mysql:host=localhost; dbname=www; charset=utf8', 'pasilehtinen');
        $stmt = $db->prepare("INSERT INTO GameStatistics (uid, level, result) values (:value0, :value1, :value2)");
        $stmt->execute(array(":value0" => $_SESSION['uid'], ":value1" => $_POST['level'], ":value2" => $_POST['newGameResult']));
    
    }
    
?>

