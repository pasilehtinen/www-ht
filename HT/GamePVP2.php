<!--Created by Pasi Lehtinen 05/12/15-->
<?php 
session_start();
if(!isset($_SESSION['uid']))
{
    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/GameMenu.php");
    exit();
    /*If user has not logged in they can't see this site --> Automatically redirect to login site*/
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Astronauttien kaksintaistelu </title>
        <meta name="description" content ="Kiva astronauttipeli! Kumpi on taitavampi astronautti? 
        Haasta kaverisi kaksintaisteluun ja selvitä kummasta tulee seuraavan avaraluusaluksen kapteeni!"/>
        <link type="text/css" rel="stylesheet" href="Muistikirja.css"/>
        <script type="text/javascript" src="Phaser/js/phaser.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="scripts/jquery.js"></script>
 
    </head>
    <body>
    <script type="text/javascript">
        /*var game = new Phaser.Game(800, 600, Phaser.AUTO, '', {preload: preload, create: create, update: update});*/
        /*var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, '', {preload: preload, create: create, update: update});
        var gW = window.innerWidth * window.devicePixelRatio * 0.75;
        var gH = window.innerHeight * window.devicePixelRatio;*/
        var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, '', {preload: preload, create: create, update: update});
        var gW = window.innerWidth;
        var gH = window.innerHeight;
        
        var platforms;
        var players;
        var player1;
        var player2;
        var beams;
        var cursors;
        var gameEnd = false;
        var level = '<?php echo $level ?>';
        var newGame;
        var quitGame;
        var W;
        var A;
        var S;
        var D;
        var music;
        var musicPlayMute;
        
        function preload() {
            game.load.image("background", "Phaser/Assets/space.png");
            game.load.image("brick", "Phaser/Assets/land.png");
            game.load.image("player", "Phaser/Assets/yellow_ball.png");
            game.load.image("asteroid", "Phaser/Assets/asteroid7.png");
            game.load.image("score", "Phaser/Assets/jets.png");
            game.load.image("beam", "Phaser/Assets/beam.png");
            
            game.load.audio("backgroundmusic", "Phaser/Assets/audio/goaman_intro.mp3", "Phaser/Assets/audio/tommy_in_goa.mp3");
            /*game.load.audio("backgroundmusic", "Phaser/Assets/audio/sd-ingame1.wav", "Phaser/Assets/audio/sd-ingame1.wav");*/
            
            cursors = game.input.keyboard.createCursorKeys();
            newGame = game.input.keyboard.addKey(Phaser.Keyboard.Y);
            quitGame = game.input.keyboard.addKey(Phaser.Keyboard.N);
            musicPlayMute = game.input.keyboard.addKey(Phaser.Keyboard.M);
            W = game.input.keyboard.addKey(Phaser.Keyboard.W);
            A = game.input.keyboard.addKey(Phaser.Keyboard.A);
            S = game.input.keyboard.addKey(Phaser.Keyboard.S);
            D = game.input.keyboard.addKey(Phaser.Keyboard.D);
            /*enable player1 & player2 keyboard*/
        
            
        }
        
        function create() {
            game.physics.startSystem(Phaser.Physics.ARCADE);
            
            game.add.tileSprite(0,0, gW, gH, "background");
            
            platforms = game.add.group();
            platforms.enableBody = true;
            
            
            players = game.add.group();
            players.enableBody = true;

            
            player1 = players.create(gW/10, gH/2, "player");
            player1.body.collideWorldBounds = true;
            player2 = players.create(gW-gW/10, gH/2, "player");
            player2.body.collideWorldBounds = true;
            
            beams = game.add.group();
            beams.enableBody=true;
            
            this.time.now = 0;
            this.nextBeamAtP1 = 0;
            this.nextBeamAtP2 = 0;
            this.beamDelay = 2750;
            /*Players shouldn't be able to shoot continuously 
            --> By changing the delay you can adjust how often they're able to shoot*/
            
            gameEnd = false;
            
            music = game.add.audio("backgroundmusic");
            if (typeof (music.loop) == 'boolean') {
                music.loop = true;
            }
            else {
                music.addEventListener('ended', function() {
                    music.currentTime = 0;
                    music.play();
                }, false);
            }
            music.play();
            /*still not looping the music..*/

        }
        
        
        function update() {
            game.physics.arcade.collide(players, platforms);
            game.physics.arcade.overlap(player1, beams, player1Die, null, this);
            game.physics.arcade.overlap(player2, beams, player2Die, null, this);
            
            player1.body.velocity.x = 0;
            player2.body.velocity.x = 0;
            if(cursors.up.isDown) {
                player1.body.velocity.y = -200;
            }
            else if(cursors.down.isDown) {
                player1.body.velocity.y = 200;
            } else {
                player1.body.velocity.y = 0;
            }
            
            if (cursors.right.isDown && !gameEnd) {
                /*Player1 shoots if the game is still ongoing*/
                if(this.nextBeamAtP1 < this.time.now) {
                    /*Beam will be fired from the middle of the sprite 
                    if the certain timeframe has exceeded (delay between beams)*/
                    this.nextBeamAtP1 = this.time.now + this.beamDelay;
                    var y = player1.position.y + player1.body.height/2;
                    var x = player1.position.x + player1.body.width;
                    var beam = beams.create(x, y, "beam");
                    beam.body.velocity.x = 250;
                }
            }
            
            if(W.isDown) {
                player2.body.velocity.y = -200;
            }
            else if(S.isDown) {
                player2.body.velocity.y = 200;
            } else {
                player2.body.velocity.y = 0;
            }
            
            if (A.isDown && !gameEnd) {
                /*Player2 shoots if the game is still ongoing*/
                if(this.nextBeamAtP2 < this.time.now) {
                    /*Beam will be fired from the middle of the sprite 
                    if the certain timeframe has exceeded (delay between beams)*/
                    this.nextBeamAtP2 = this.time.now + this.beamDelay;
                    var y = player2.position.y + player2.body.height/2;
                    var x = player2.position.x - player2.body.width;
                    var beam = beams.create(x, y, "beam");
                    beam.body.velocity.x = -250;
                }
            }
            
            
            if (musicPlayMute.isDown) {
                /*isDown event not working properly.. need another event for this one*/
                if(music.paused) {
                    music.resume();
                } else {
                    music.pause();
                }
            }
            
            if (newGame.isDown && gameEnd) {
                /*If the game has ended (other player died) they can take a rematch
                --> Modify game to beginning state:
                    - Resurrect players
                    - Enable shooting etc.*/
                gameEnd = false;
                
                    player1.destroy();
                    player2.destroy();

                    /*create new sprites*/
                    player1 = game.add.sprite(gW/10, gH/2, "player");
                    game.physics.arcade.enable(player1);
                    player1.body.gravity.y = 0;
                    player1.body.collideWorldBounds = true;
                    
                    player2 = game.add.sprite(gW-gW/10, gH/2, "player");
                    game.physics.arcade.enable(player2);
                    player2.body.gravity.y = 0;
                    player2.body.collideWorldBounds = true;
                
                this.nextBeamAtP1 = 0;
                this.nextBeamAtP2 = 0;

                gameEndText.text = "";

            }
            if (quitGame.isDown && gameEnd) {
                window.location.href = "https://www-ht-pasilehtinen.c9users.io/HT/GameMenu.php";
                /*Redirect to Main Menu if the users don't wanna take rematch*/
            }
            

            function player1Die (player1, beam) {
                /*beam has hit player1 --> player2 wins*/
                player1.destroy();
                player2.destroy();
                gameEnd = true;
                gameEndText = game.add.text(gW/2-gW/10, gH/2, "Pelaaja 2 voitti! Uusi peli (Y/N)?", {fontSize: '32px', fill: '#fff'});
            }
            function player2Die (player2, beam) {
                /*beam has hit player2 --> player1 wins*/
                player2.destroy();
                player1.destroy();
                gameEnd = true;
                gameEndText = game.add.text(gW/2-gW/10, gH/2, "Pelaaja 1 voitti! Uusi peli (Y/N)?", {fontSize: '32px', fill: '#fff'});
            }
            
        }
        
        
        
    </script>
    </body>
</html>