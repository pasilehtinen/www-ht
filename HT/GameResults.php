<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();


if(!isset($_SESSION['uid']))
{
    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/login.php");
    exit();
    /*If user has not logged in they can't see this site --> Automatically redirect to login site*/

}

header('Content-type: text/html; charset=utf-8');
    

        $db = new PDO('mysql:host=localhost; dbname=www; charset=utf8', 'pasilehtinen');
        $stmt2 = $db->prepare("
        Select 
            username, 
            level 
        from GameStatistics join users 
        on GameStatistics.uid = users.uid 
        where result = 1 and level > 0 
        order by level desc, DateTime asc limit 10");
        /*Combine result data with user data - retrieve results based on user name*/
        $stmt2->execute();
        $rows2 = $stmt2->fetchALL(PDO::FETCH_ASSOC);
    
        print(json_encode($rows2));

?>