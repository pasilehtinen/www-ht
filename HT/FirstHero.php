<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();


if(!isset($_SESSION['uid']))
{
    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/login.php");
    exit();
    /*If user has not logged in they can't see this site --> Automatically redirect to login site*/

}

header('Content-type: text/html; charset=utf-8');
    
        /*This information can't be changed after someone has accomplished this once
        --> Usage of chache is a very good idea
        Cache could be reseted once per hour*/
        $mem = new Memcached();
        $mem->addServer(localhost, 11211) or die("Unable to connect");
        
        
        $result = $mem->get("FirstHero");
        /*$mem->delete("FirstHero");
        */
        
        if(!$result) {
            $db = new PDO('mysql:host=localhost; dbname=www; charset=utf8', 'pasilehtinen');
            $stmt2 = $db->prepare("
            Select 
                username, 
                DateTime
            from GameStatistics join users 
            on GameStatistics.uid = users.uid 
            where result = 1 and level = 0 
            order by DateTime asc limit 1");
  
            $stmt2->execute();
            $rows2 = $stmt2->fetchALL(PDO::FETCH_ASSOC);
            /*$hero = $rows2[0]['username'] + " ("+$rows2[0]['DateTime']+")";*/

            /*$hero = $rows2[0]['username']+" "+$rows2[0]['DateTime']->format('Y-m-d H:i:s');*/
            $hero = $rows2[0]['username'];
            
            $mem->set("FirstHero", $hero, 60*60*24);
            $result = $mem->get("FirstHero");
 
        } 
        
        print(json_encode($result));

?>