<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();
$hash = 'lsjJDOIH€#FSf0289>49024_S:_SsFLMfJIf092';
header('Content-type: text/html; charset=utf-8');

    if (isset($_POST['username']) && isset($_POST['password'])) {
        $uname = $_POST['username'];
        $pw = $_POST['password'];
        $pwhash=password_hash($pw, PASSWORD_BCRYPT, array('cost'=>12));
        $loginmessage = "";
        
        $db = new PDO('mysql:host=localhost; dbname=www; charset=utf8', 'pasilehtinen');
        $stmt7 = $db->prepare("Select * from users where username = (:username)");
        $stmt7->execute(array(":username" => $_POST['username']));
        $rows7 = $stmt7->fetchALL(PDO::FETCH_ASSOC);
        if (password_verify($pw, $rows7[0]['pwhash'])) {
            $_SESSION['uid'] =  $rows7[0]['uid'];
            $_SESSION['admin'] =  $rows7[0]['admin'];
            
            $loginmessage = "Kirjautuminen on onnistunut - Olet kirjautuneena sisälle";
            $_SESSION['loginmessage'] = $loginmessage;
            header("Location: https://www-ht-pasilehtinen.c9users.io/HT/GameMenu.php");
            exit();
            /*print(json_encode($loginmessage));
            return true;*/
            /*In case of correct login information --> redirect to Main Menu*/
        } else {
            $loginmessage = "Väärä käyttäjätunnus tai salasana";
            $_SESSION['loginmessage'] = $loginmessage;
            header("Location: https://www-ht-pasilehtinen.c9users.io/HT/login.php");
            exit();
            /*In case of wrong login autentication --> Error message*/
            /*redirect to login page*/
        }
        
    } else if (isset($_POST['newusername']) && isset($_POST['newpassword'])) {
        
        $newuname = $_POST['newusername'];
        $newpw = $_POST['newpassword'];
        $newpwhash=password_hash($newpw, PASSWORD_BCRYPT, array('cost' => 12));
        
        $db = new PDO('mysql:host=localhost; dbname=www; charset=utf8', 'pasilehtinen');
        $stmt4 = $db->prepare("Select count(*) as qty from users where username = (:username)");
        $stmt4->execute(array(":username" => $_POST['newusername']));
        $username_free = $stmt4->fetchALL(PDO::FETCH_ASSOC);
        
        if (strlen($newpw)>=8 && strlen($newpw)<=255 ) {
            /*password should be between 8 and 255 chars length*/
            if (!preg_match("/[\'^£$%&*()}{@#~?><>,|=_+¬-]/",$newpw)) {
                /*password shouldn't include special characters*/
                if (preg_match('/[a-z]/', $newpw) && preg_match('/[A-Z]/', $newpw) && preg_match('/[0-9]/', $newpw)) {
                    /*password should containt small charactes, capital characters and numbers as well*/
                    if ($username_free[0]['qty'] === '0') {
                        /*username should be unique --> check if available from database*/

                        $stmt5 = $db->prepare("insert into users (username, pwhash) values(:username, :pwhash)");
                        $stmt5->execute(array(":username" => $newuname, ":pwhash" => $newpwhash));
                        
                        $stmt6 = $db->prepare("Select * from users order by uid desc limit 1;");
                        $stmt6->execute();
                        $uid = $stmt6->fetchALL(PDO::FETCH_ASSOC);
                        $_SESSION['uid'] =  $uid[0]['uid'];
                        $_SESSION['admin'] =  $uid[0]['admin'];
                        
                        /*Stored null result for each level of the game 1-10 in order to retrieve result list though no level has played yet*/
                        $stmt7 = $db->prepare("INSERT INTO GameStatistics (uid, level, result) values 
                        (:value0, :value1, :value11),
                        (:value0, :value2, :value11),
                        (:value0, :value3, :value11),
                        (:value0, :value4, :value11),
                        (:value0, :value5, :value11),
                        (:value0, :value6, :value11),
                        (:value0, :value7, :value11),
                        (:value0, :value8, :value11),
                        (:value0, :value9, :value11),
                        (:value0, :value10, :value11)
                        ");
                        $stmt7->execute(array(
                            ":value0" => $_SESSION['uid'], 
                            ":value1" => 1,
                            ":value2" => 2,
                            ":value3" => 3,
                            ":value4" => 4,
                            ":value5" => 5,
                            ":value6" => 6,
                            ":value7" => 7,
                            ":value8" => 8,
                            ":value9" => 9,
                            ":value10" => 0,
                            ":value11" => null));
                        
                        $subscribemessage = "Rekisteröityminen on onnistunut - Olet kirjautuneena sisälle";
                        $_SESSION['subscribemessage'] = $subscribemessage;
                        header("Location: https://www-ht-pasilehtinen.c9users.io/HT/GameMenu.php");
                        exit();
                        /*When subscribe accomplishes --> redirect to Main Menu
                        Otherwise: give necessary error message*/
                    } else {
                        $subscribemessage = "Valitsemasi käyttäjätunnus on varattu";
                        $_SESSION['subscribemessage'] = $subscribemessage;
                        header("Location: https://www-ht-pasilehtinen.c9users.io/HT/newuser.php");
                        exit();
                    }
                } else {
                    $subscribemessage = "Salasanassa tulee olla sekä isoja että pieniä kirjaimia ja numeroita";
                    $_SESSION['subscribemessage'] = $subscribemessage;
                    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/newuser.php");
                    exit();
                }
            } else {
                $subscribemessage = "Salasanan voi sisältää vain kirjaimia ja numeroita";
                $_SESSION['subscribemessage'] = $subscribemessage;
                header("Location: https://www-ht-pasilehtinen.c9users.io/HT/newuser.php");
                exit();
            }
        } else {
            $subscribemessage = "Salasanan tulee olla vähintään 8 merkkiä, mutta enintään 255 merkkiä";
            $_SESSION['subscribemessage'] = $subscribemessage;
            header("Location: https://www-ht-pasilehtinen.c9users.io/HT/newuser.php");
            exit();
        }
    }    
    
?>