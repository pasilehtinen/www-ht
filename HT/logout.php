<!--Created by Pasi Lehtinen 05/12/15-->
<?php
session_destroy();
session_start();
$_SESSION = array();
session_destroy();
/*Clean session array*/
header("Location: https://www-ht-pasilehtinen.c9users.io/HT/login.php");
/*After logout automatically redirect to login site*/
exit();
?>