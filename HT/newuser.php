<?php
/*Created by Pasi Lehtinen 05/12/15*/
session_start();
header('Content-type: text/html; charset=utf-8');
?>


<html>
    <head>
        <title> Kiva Peli </title>
        <link type="text/css" rel="stylesheet" href="GameFormatting.css"/>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        
        <script type="text/javascript">

            function checkAuthentication() {
                var uname=document.forms["login"]["username"].value;
                var pw=document.forms["login"]["password"].value;
                var addRequest = $.ajax({
                    url: "LoginSubscribeHandler.php",
                    type: "POST",
                    data: {
                        "username": uname,
                        "password": pw
                    },
                    success: function (message) {

                        document.getElementById("loginmessage").innerHTML = message;
                        return message;
                    },
                    dataType: "html"
                });
                
            }
            
        </script>
    </head>
    <body>
        
        <div id="header">
            <h1>Rekisteröityminen</h1>
            <br><br>
            
            <form action="https://www-ht-pasilehtinen.c9users.io/HT/login.php" method="post">
                    <input type="submit" value ="Kirjautuminen"/>
            </form>
            
            <a><?php echo  $_SESSION['subscribemessage']?></a>
            <br><br>
            <div>
                <form name = "subscribe" action="https://www-ht-pasilehtinen.c9users.io/HT/LoginSubscribeHandler.php" method="post">
                    Käyttäjätunnus: <input type="text" name="newusername"></input>
                    Salasana: <input type="password" name="newpassword"></input>
                    <input type="submit"/>
                </form>
            </div>
        </div>
        
    </body>
</html>
