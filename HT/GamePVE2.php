<!--Created by Pasi Lehtinen 05/12/15-->
<?php 
session_start();
if(!isset($_SESSION['uid']) || !isset($_POST['level']))
{
    header("Location: https://www-ht-pasilehtinen.c9users.io/HT/GameMenu.php");
    exit();
    /*If user has not logged in they can't see this site --> Automatically redirect to login site*/
}
header('Content-type: text/html; charset=utf-8');

    if(isset($_POST['level'])) {
        $level = $_POST['level'];
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Astronautin seikkailupeli </title>
        <meta name="description" content ="Kiva astronauttipeli! Sinun pitää kerätä tähtipölyä universumin pelastamiseksi. 
        Varo lähestyviä meterotiitteja tai aluksesi tuhoutuu. Pääsetkö maaliin asti? Muista, että avaruus voi tuoda 
        eteesi myös vihamielisiä avaruusolioita! Kokeile kuinka hyvä astronautti sinä olisit!"/>
        <script type="text/javascript" src="Phaser/js/phaser.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="scripts/jquery.js"></script>
        <script>
            function AddNewResult(result, level) {
                /*Send result of the game to GameHandler which stores it to the database*/
                var addRequest = $.ajax({
                    url: "GameHandler.php",
                    type: "POST",
                    data: {
                        "newGameResult": result,
                        "level": level
                    },
                    dataType: "html"
                });
            }
        </script>
    </head>
    <body>
    <script type="text/javascript">
        /*var game = new Phaser.Game(800, 600, Phaser.AUTO, '', {preload: preload, create: create, update: update});*/
        /*var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, '', {preload: preload, create: create, update: update});*/
        var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, '', {preload: preload, create: create, update: update});
        
        /*var gW = window.innerWidth * window.devicePixelRatio * 0.75;
        var gH = window.innerHeight * window.devicePixelRatio;
        */
        var gW = window.innerWidth;
        var gH = window.innerHeight;
        
        var platforms;
        var player;
        var boss;
        var boss_health = 3;
        var beams;
        var cursors;
        var asteroids;
        var scores;
        var enemyBullets;
        var points = 0;
        var gameEnd = false;
        var level = '<?php echo $level ?>';
        var newGame;
        var quitGame;
        var music;
        var musicPlayMute;
        var win = false;

        
        function preload() {
            
            game.load.image("background", "Phaser/Assets/space.png");
            game.load.image("brick", "Phaser/Assets/land.png");
            game.load.image("player", "Phaser/Assets/ship.png");
            game.load.image("asteroid", "Phaser/Assets/asteroid7.png");
            game.load.image("score", "Phaser/Assets/jets.png");
            game.load.image("beam", "Phaser/Assets/beam.png");
            game.load.image("boss", "Phaser/Assets/final_boss.png");
            game.load.image("boss_bullet", "Phaser/Assets/orb-green.png");
            
            /*game.load.audio("backgroundmusic", "Phaser/Assets/audio/goaman_intro.mp3", "Phaser/Assets/audio/tommy_in_goa.mp3");*/
            game.load.audio("backgroundmusic", "Phaser/Assets/audio/sd-ingame1.wav", "Phaser/Assets/audio/sd-ingame1.wav");
            
            cursors = game.input.keyboard.createCursorKeys();
            newGame = game.input.keyboard.addKey(Phaser.Keyboard.Y);
            quitGame = game.input.keyboard.addKey(Phaser.Keyboard.N);
            musicPlayMute = game.input.keyboard.addKey(Phaser.Keyboard.M);
        
            
        }
        
        function create() {
            game.physics.startSystem(Phaser.Physics.ARCADE);
            
            game.add.tileSprite(0,0, gW, gH, "background");
            
            platforms = game.add.group();
            platforms.enableBody = true;
            
            /*var ground = platforms.create(0, 435, "brick");
            ground.scale.setTo(2,2);
            ground.body.immovable = true;*/
            
            player = game.add.sprite(gW/10, gH/2, "player");
            game.physics.arcade.enable(player);
            player.body.gravity.y = 0;
            player.body.collideWorldBounds = true;
            
            asteroids = game.add.group();
            asteroids.enableBody = true;
            
            enemyBullets = game.add.group();
            enemyBullets.enableBody = true;
            
            beams = game.add.group();
            beams.enableBody=true;
            
            scores = game.add.group();
            scores.enableBody=true;
            
            this.time.now = 0;
            this.nextAsteroidAt = 0;
            this.asteroidDelay = 1000;
            this.nextBeamAt = 0;
            this.nextEnemyBullet = 500;
            this.beamDelay = 2750;
            this.nextScoreAt = 5000;
            this.scoreDelay = 10000;
            this.enemyBulletDelay = 2000;
            this.wave = 1;
            points = 0;
            gameEnd = false;
            /*Players shouldn't be able to shoot continuously 
            --> By changing the delay you can adjust how often they're able to shoot
            Same applies to Asteroids and enemy bullets; 
            Though more asterois will be spawned when the player gets further in the space (linear)*/
            
            if (level < 10) {
                /*The goal is to collect star dust (points)*/
                scoreText = game.add.text(1, 1, "0/" + level, {fontSize: '10px', fill: '#fff'});
            } else if (level == 10) {
                /*The goal is to hit the boss three times - shooting enabled only when star dust is collected*/
                scoreText = game.add.text(1, 1, "Final Boss - ammonation: 0", {fontSize: '10px', fill: '#fff'});
                boss = game.add.sprite(gW-gW/10, gH/2, "boss");
                game.physics.arcade.enable(boss);
                boss.body.velocity.y = 350;
                
                boss.body.collideWorldBounds = true;
                boss.body.bounce.set(1);
                boss_health = 3;
            } else {
                scoreText = game.add.text(1, 1, "0", {fontSize: '10px', fill: '#fff'});
            }
            
            music = game.add.audio("backgroundmusic");
            if (typeof (music.loop) == 'boolean') {
                music.loop = true;
            }
            else {
                music.addEventListener('ended', function() {
                    music.currentTime = 0;
                    music.play();
                }, false);
            }
            music.play();
            /*still not looping the music..*/

        }
        
        
        function update() {
            game.physics.arcade.collide(player, platforms);
            game.physics.arcade.overlap(player, asteroids, playerDie, null, this);
            game.physics.arcade.overlap(player, enemyBullets, playerDie, null, this);
            game.physics.arcade.overlap(beams, asteroids, asteroidHit, null, this);
            game.physics.arcade.overlap(player, scores, increaseScore, null, this);
            game.physics.arcade.overlap(beams, boss, bossHit, null, this);
            
            player.body.velocity.x = 0;
            if(cursors.up.isDown) {
                /*Move up*/
                player.body.velocity.y = -200;
            }
            else if(cursors.down.isDown) {
                /*move down*/
                player.body.velocity.y = 200;
            } else {
                player.body.velocity.y = 0;
            }
            
            if (cursors.right.isDown && !gameEnd) {
                /*Shoot beams if game is still ongoing*/
                if(this.nextBeamAt < this.time.now) {
                    /*Three beams will be fired if the timeframe has exceeded the beamdelay since the last fired beam*/
                    if (points > 0 || level != 10) {
                        /*Shooting enabled in levels 0-9 & 11-n no matter if the user has ammonation*/
                        if (level == 10) {
                            /*In level 10 user can shoot only when he has ammonation (star dust collected)*/
                            /*In level 10 only 1 beam will be fired*/
                            points--; 
                            scoreText.text = "Final boss - ammonation: " + points;
                            this.nextBeamAt = this.time.now + this.beamDelay;
                            var y = player.position.y + player.body.height/2;
                            var x = player.position.x + player.body.width/2;
                            var beam = beams.create(x, y, "beam");
                            beam.body.velocity.x = 500;
                        } else {
                            /*Three beams will be fired in levels 0-9 & 11-n*/
                            this.nextBeamAt = this.time.now + this.beamDelay;
                            var y = player.position.y + player.body.height/2;
                            var x = player.position.x + player.body.width/2;
                            var beam = beams.create(x, y, "beam");
                            beam.body.velocity.x = 250;
                            var beam = beams.create(x, y+beam.body.height, "beam");
                            beam.body.velocity.x = 250;
                            var beam = beams.create(x, y-beam.body.height, "beam");
                            beam.body.velocity.x = 250;
                        }
                    }
                    
                    
                }
            } 
            
            if (musicPlayMute.isDown) {
                /*isDown event not working properly.. need another event for this one*/
                if(music.paused) {
                    music.resume();
                } else {
                    music.pause();
                }
            }
            
            if (newGame.isDown && gameEnd) {
                /*window.location.href = "https://www-sovellukset-pasi-pasilehtinen.c9.io/Game.php";*/
                gameEnd = false;
                
                if(win) {
                    /*Level increased by one 
                    (applies to levels 0-10 since level 11 is infinite game - survive as long as possible)*/
                    level++;
                    player.destroy();
                }
                    player = game.add.sprite(gW/10, gH/2, "player");
                    game.physics.arcade.enable(player);
                    player.body.gravity.y = 0;
                    player.body.collideWorldBounds = true;
                
                
                this.nextAsteroidAt = 0;
                this.nextBeamAt = 0;
                this.wave = 1;
                points = 0;
                /*reset delays and points*/
                
                if (level < 10) {
                    scoreText.text = "0/" + level;
                } else if (level == 10) {
                    /*spawn boss if level 10 is reached*/
                    scoreText.text = "Final boss - ammonation: 0";
                    boss_health = 3;
                    boss = game.add.sprite(gW-gW/10, gH/2, "boss");
                    game.physics.arcade.enable(boss);
                    boss.body.velocity.y = 350;
                    boss.body.collideWorldBounds = true;
                    boss.body.bounce.set(1);
                } else if (level > 10) {
                    /*Survival level - no goal --> just try to get as many scores as possible and reach new high score*/
                    scoreText.text = "0"
                } 
                
                gameEndText.text = "";
                win = false;
                asteroids = game.add.group();
                asteroids.enableBody = true;
                beams = game.add.group();
                beams.enableBody = true;
                scores = game.add.group();
                scores.enableBody = true;
                /*enable every game particles which were destroyed in the end of previous game*/
                
            }
            if (quitGame.isDown && gameEnd) {
                window.location.href = "https://www-ht-pasilehtinen.c9users.io/HT/GameMenu.php";
                /*Redirect player to Main Menu if he doesn't want to continue after loosing*/
            }
            
            if (this.nextAsteroidAt < this.time.now && !gameEnd) {
                this.nextAsteroidAt = this.time.now + this.asteroidDelay;
                /*Spawn more asteroids if the game is ongoing and the delay between asteroid spawns has exceeded*/
                for (var j=0; j<Math.round(this.wave/10); j++) {
                    var randY = Math.round(Math.random()*(gH - 0) + 0);
                    /*game.world.randomX*/
                    var asteroid = asteroids.create(gW, randY, "asteroid");
                    asteroid.body.gravity.x = -50 - Math.random()*10;
                }

                this.wave++;
                /*Increase the wave counter by one: this defined how many asteroids are spawned at the time*/
            }
            if (this.nextScoreAt < this.time.now && !gameEnd) {
                this.nextScoreAt = this.time.now + this.scoreDelay;
                var randomY = Math.round(Math.random()*(gH - 0) + 0);
                /*game.world.randomX*/
                var score = scores.create(gW, randomY, "score");
                score.body.gravity.x = -50 - Math.random()*10;
                /*Same principles applied to scores as for the asteroids though only 1 score spawned at once*/
            }
            if (this.nextEnemyBullet < this.time.now && !gameEnd && level == 10) {
                this.nextEnemyBullet = this.time.now + this.enemyBulletDelay;
                var enemy_Y = boss.position.y + boss.body.height/2;
                var enemy_X = boss.position.x + boss.body.width/2;
                var enemyBullet = enemyBullets.create(enemy_X, enemy_Y, "boss_bullet");
                enemyBullet.body.gravity.x = -500;
                /*The boss will also shoot the player in level 10*/
            }
            
            
            function asteroidHit (beam, asteroid) {
                /*destroy each asteroid that the beam hits*/
                asteroid.destroy();
            }
            
            function bossHit(boss, beam) {
                /*Decrease boss health and make the boss body smaller (harder to hit) and at last destroy boss*/
                beam.destroy();
                if (boss_health==3) {
                    boss.scale.setTo(0.5,0.5);
                    boss_health--;
                } else if (boss_health == 2) {
                    boss.scale.setTo(0.25,0.25);
                    boss_health--;
                } else if (boss_health == 1) {
                    boss.destroy();
                    boss_health--;
                    gameEnd = true;
                    AddNewResult(1, 0);
                    /*Boss defeats stored with score '0' because actually 10 star dusts were not collected
                    This must be known when result sets are retrieved from database*/
                    player.body.collideWorldBounds = false;
                    player.body.velocity.x = 99999;
                    player.body.gravity.x = 99999;
                    gameEndText = game.add.text(gW/2-gW/10, gH/2, "Voitit! Seuraava taso (Y/N)?", {fontSize: '32px', fill: '#fff'});
                    asteroids.destroy();
                    /*beams.destroy();*/
                    win = true;
                    
                        enemyBullets.destroy();
                   
                }
                
            }
            
            function playerDie (player, asteroid) {
                /*Player dies when he gets hitted by asteroid*/
                asteroids.destroy();
                player.destroy();
                gameEnd = true;
                beams.destroy();
                scores.destroy();
                if (level > 10 && points > 10) {
                    /*New high score is possible --> Save result as "won" to the database*/
                    AddNewResult(1, points);
                } else {
                    /*Player has lost the game and the result is saved as "lost"*/
                    AddNewResult(0, level);
                }
                
                if (level == 10) {
                    boss.destroy();
                }
                
                gameEndText = game.add.text(gW/2-gW/10, gH/2, "Hävisit! Uusi peli (Y/N)?", {fontSize: '32px', fill: '#fff'});
            }
            function increaseScore(player, score) {
                points++;
                if (level < 10) {
                    /*Increase points by one*/
                    scoreText.text = points + "/" + level;
                } else if (level == 10) {
                    /*Increase ammonation by one*/
                    scoreText.text = "Final boss - ammonation: " + points;
                } else if (level > 10) {
                    /*Increase points by one*/
                    scoreText.text = points;
                }
                
                score.destroy();
                if (points >= level && level < 10) {
                    /*Game ends, player wins, result saved to the database*/
                    gameEnd = true;
                    AddNewResult(1, level);
                    player.body.collideWorldBounds = false;
                    player.body.velocity.x = 99999;
                    player.body.gravity.x = 99999;
                    gameEndText = game.add.text(gW/2-gW/10, gH/2, "Voitit! Seuraava taso (Y/N)?", {fontSize: '32px', fill: '#fff'});
                    asteroids.destroy();
                    win = true;
                }
                
            }
            
        }
        
        
        
    </script>
    </body>
</html>